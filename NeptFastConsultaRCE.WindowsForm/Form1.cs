﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NeptFastConsultaRCE.WindowsService;

namespace NeptFastConsultaRCE.WindowsForm
{
    public partial class Form1 : Form
    {
        Execution objExecution = new Execution();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            objExecution.Ejecutar();
        }
    }
}
