﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using NeptFastConsultaRCE.BusinessEntity;
using System.Data.SqlClient;
using System.Data;


namespace NeptFastConsultaRCE.DataAccess
{
    public class CosettingDA
    {
        string CadenaConexion = ConfigurationManager.ConnectionStrings["Terminal"].ToString();

        public string ObtenerCosetting(string prKey)
        {
            string viNameStore = string.Empty;
            string viSQLDebug = string.Empty;
            string ValorCosetting = string.Empty;

            using (SqlConnection SQLCon = new SqlConnection(CadenaConexion))
            {
                using (SqlCommand SQLCmd = new SqlCommand())
                {
                    SQLCmd.Connection = SQLCon;
                    SQLCmd.CommandType = CommandType.StoredProcedure;
                    SQLCmd.CommandText = "sp_IntN4_ObtenerValorCosetting";
                    SQLCmd.CommandTimeout = 0;

                    #region 01. Parametros de Ingreso
                    SQLCmd.Parameters.Add(new SqlParameter("@KeyCosetting", SqlDbType.VarChar)).Value = prKey;
                    #endregion

                    if (SQLCmd.Connection.State == ConnectionState.Closed) { SQLCmd.Connection.Open(); }

                    using (SqlDataReader viSQLDr = SQLCmd.ExecuteReader(CommandBehavior.SingleResult))
                    {
                        if (viSQLDr.HasRows)
                        {
                            while (viSQLDr != null && viSQLDr.Read())
                            {
                                ValorCosetting = viSQLDr[0].ToString();
                            }
                        }
                    }
                    SQLCmd.Parameters.Clear();
                    if (SQLCmd.Connection.State == ConnectionState.Closed) { SQLCmd.Connection.Close(); }
                }
            }

            return ValorCosetting;
        }

    }
}
