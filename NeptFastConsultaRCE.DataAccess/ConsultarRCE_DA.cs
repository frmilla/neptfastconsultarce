﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using NeptFastConsultaRCE.BusinessEntity;
using System.Data.SqlClient;
using System.Data;

namespace NeptFastConsultaRCE.DataAccess
{
    public class ConsultarRCE_DA
    {
        string ConexionDescargaPaita = ConfigurationManager.ConnectionStrings["DescargaPaita"].ToString();
        string ConexionDescarga = ConfigurationManager.ConnectionStrings["Descarga"].ToString();
        string ConexionTerminal = ConfigurationManager.ConnectionStrings["Terminal"].ToString();

        public DataTable ConsultarTicketPendientesRevision(string viSucursal)
        {
            string Cadena = string.Empty;
            if (viSucursal == "PAI")
            {
                Cadena = ConexionDescargaPaita;
            }
            else
            {
                Cadena = ConexionDescarga;
            }

            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();

            SqlConnection CON = new SqlConnection(Cadena);
            cmd.Connection = CON;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_IntN4_FAST_ConsultarRCE";
            cmd.CommandTimeout = 0;

            CON.Open();
            cmd.ExecuteNonQuery();
            CON.Close();

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;
        }

        public void UpdateEstadoTicketRCE(string viSucursal, string viNroTticket, string viEstado)
        {
            string Cadena = string.Empty;
            if (viSucursal == "PAI")
            {
                Cadena = ConexionDescargaPaita;
            }
            else
            {
                Cadena = ConexionDescarga;
            }

            SqlConnection CON = new SqlConnection(Cadena);
            SqlCommand cmd = new SqlCommand();

            cmd.Connection = CON;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_IntN4_FAST_UpdateEstadoTicketRCE";
            cmd.Parameters.Add("@NroTicket", SqlDbType.VarChar).Value = viNroTticket;
            cmd.Parameters.Add("@Estado", SqlDbType.VarChar).Value = viEstado;
            cmd.CommandTimeout = 0;

            CON.Open();
            cmd.ExecuteNonQuery();
            CON.Close();
        }

        public void EnvioCorreoMasivoPendientes()
        {
            SqlConnection CON = new SqlConnection(ConexionTerminal);
            SqlCommand cmd = new SqlCommand();

            cmd.Connection = CON;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_IntN4_FAST_EnviarCorreoMasivoPendientesRCE";
            cmd.CommandTimeout = 0;

            CON.Open();
            cmd.ExecuteNonQuery();
            CON.Close();
        }


    }
}
