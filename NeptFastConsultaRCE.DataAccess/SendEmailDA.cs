﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeptFastConsultaRCE.BusinessEntity;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace NeptFastConsultaRCE.DataAccess
{
    public class SendEmailDA
    {
        string CadenaConexion = ConfigurationManager.ConnectionStrings["Terminal"].ToString();

        public void Send(string sError, string sTraza)
        {
            SqlConnection CON = new SqlConnection(CadenaConexion);
            SqlCommand cmd = new SqlCommand();

            cmd.Connection = CON;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_IntN4_FAST_EnviarErrorConsultarRCE";
            cmd.Parameters.Add("@viError", SqlDbType.VarChar).Value = sError;
            cmd.Parameters.Add("@viTraza", SqlDbType.VarChar).Value = sTraza;
            cmd.CommandTimeout = 0;

            CON.Open();
            cmd.ExecuteNonQuery();
            CON.Close();
        }
    }
}
