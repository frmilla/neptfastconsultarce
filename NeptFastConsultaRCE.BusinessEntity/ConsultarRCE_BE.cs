﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeptFastConsultaRCE.BusinessEntity;

using System.Data;
using System.Data.SqlClient;

namespace NeptFastConsultaRCE.BusinessEntity
{
    public class ConsultarRCE_BE
    {
        protected string _NroTicket;
        protected string _Contenedor;
        protected string _Tipo;
        protected string _Manifiesto;
        protected string _Oficina;
        protected string _Estado;

        public string NroTicket { get { return _NroTicket; } set { _NroTicket = value; } }
        public string Contenedor { get { return _Contenedor; } set { _Contenedor = value; } }
        public string Tipo { get { return _Tipo; } set { _Tipo = value; } }
        public string Manifiesto { get { return _Manifiesto; } set { _Manifiesto = value; } }
        public string Oficina { get { return _Oficina; } set { _Oficina = value; } }
        public string Estado { get { return _Estado; } set { _Estado = value; } }
    }
}
