﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeptFastConsultaRCE.BusinessEntity
{
    public class SendEmailBE
    {
        protected string _Error;
        protected string _Traza;

        public string Error { get { return _Error; } set { _Error = value; } }
        public string Traza { get { return _Traza; } set { _Traza = value; } }
    }
}
