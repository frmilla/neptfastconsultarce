﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeptFastConsultaRCE.BusinessEntity;
using NeptFastConsultaRCE.DataAccess;
using System.Data;
using System.Data.SqlClient;

namespace NeptFastConsultaRCE.BusinessLogic
{
    public class SendEmailBL
    {
        SendEmailDA objSendEmailDA = new SendEmailDA();

        public void Send(string sError, string sTraza)
        {
            objSendEmailDA.Send(sError, sTraza);
        }
    }
}
