﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeptFastConsultaRCE.BusinessEntity;
using NeptFastConsultaRCE.DataAccess;
using System.Data;
using System.Data.SqlClient;


namespace NeptFastConsultaRCE.BusinessLogic
{
    public class ConsultarRCE_BL
    {
        ConsultarRCE_DA objConsultarRCE_DA = new ConsultarRCE_DA();

        public DataTable ConsultarTicketPendientesRevision(string viSucursal)
        {
            return objConsultarRCE_DA.ConsultarTicketPendientesRevision(viSucursal);
        }

        public void UpdateEstadoTicketRCE(string viSucursal, string viNroTticket, string viEstado)
        {
            objConsultarRCE_DA.UpdateEstadoTicketRCE(viSucursal, viNroTticket, viEstado);
        }

        public void EnvioCorreoMasivoPendientes()
        {
            objConsultarRCE_DA.EnvioCorreoMasivoPendientes();
        }
        
    }
}
