﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeptFastConsultaRCE.BusinessEntity;
using NeptFastConsultaRCE.DataAccess;

namespace NeptFastConsultaRCE.BusinessLogic
{
    public class CosettingBL
    {
        CosettingDA objCosettingDA = new CosettingDA();

        public string ObtenerCosetting(string prKey)
        {
            return objCosettingDA.ObtenerCosetting(prKey);
        }
    }
}
