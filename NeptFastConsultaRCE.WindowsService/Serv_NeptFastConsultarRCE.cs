﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Configuration;

namespace NeptFastConsultaRCE.WindowsService
{
    public partial class Serv_NeptFastConsultarRCE : ServiceBase
    {
        private Timer oTimer = null;
        private Execution objExecution = new Execution();
        int viMinutos = int.Parse(ConfigurationManager.AppSettings["vpMinutos"].ToString());

        public Serv_NeptFastConsultarRCE()
        {
            InitializeComponent();
            int Minutos = 0;
            Minutos = viMinutos * 60 * 1000;
            oTimer = new Timer(Minutos);
            oTimer.Elapsed += OnTimedEvent;
        }

        protected override void OnStart(string[] args)
        {
            oTimer.Start();
        }

        protected override void OnStop()
        {
            oTimer.Stop();
        }

        private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            objExecution.Ejecutar();
        }

    }
}
