﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using NeptFastConsultaRCE.BusinessEntity;
using NeptFastConsultaRCE.BusinessLogic;
using System.Net;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using Newtonsoft;

namespace NeptFastConsultaRCE.WindowsService
{
    public class Execution
    {

        string viRutaLog = ConfigurationManager.AppSettings["vpRutaLog"].ToString();
        ConsultarRCE_BL objConsultarRCE_BL = new ConsultarRCE_BL();
        ConsultarRCE_BE objConsultarRCE_BE = new ConsultarRCE_BE();
        SendEmailBL objSendEmailBL = new SendEmailBL();
        string sUserFast = string.Empty;
        string sPassFast = string.Empty;

        public void Ejecutar()
        {
            if (!File.Exists(viRutaLog)) { Directory.CreateDirectory(viRutaLog); }

            string ruta = (viRutaLog + "\\LogConsultaRCE.txt");
            TextWriter otextwriter = new StreamWriter(ruta, true);
            otextwriter.WriteLine(DateTime.Now + ": Iniciando Servicio");

            try
            {
                Ejecutar_Datos(otextwriter);
            }
            catch (Exception ex)
            {
                string sError = ex.Message.ToString().Trim();
                otextwriter.WriteLine(DateTime.Now + ": Error - " + ex.Message);

                objSendEmailBL.Send(sError, "Error General");
            }
            finally
            {
                otextwriter.WriteLine(DateTime.Now + ": Finalizando Servico");
                otextwriter.Close();
            }
        }

        protected void Ejecutar_Datos(System.IO.TextWriter otextwriter)
        {
            DatosBd(otextwriter, "PAI");
            DatosBd(otextwriter, "ARG");

            objConsultarRCE_BL.EnvioCorreoMasivoPendientes();
        }

        protected void DatosBd(System.IO.TextWriter otextwriter, string viSucursal)
        {
            DataTable DtPendientes = objConsultarRCE_BL.ConsultarTicketPendientesRevision(viSucursal);
            string sTraza = "";
            string Response = string.Empty;
            string sError = string.Empty;

            CosettingBL objCosettingBL = new CosettingBL();
            sUserFast = objCosettingBL.ObtenerCosetting("USER_DPW_FAST");
            sPassFast = objCosettingBL.ObtenerCosetting("PASS_DPW_FAST");

            ODSNeptFast.ODSNeptFast ODSNeptFastNEP = new ODSNeptFast.ODSNeptFast();

            if (DtPendientes.Rows.Count > 0)
            {
                for (int i = 0; i < DtPendientes.Rows.Count; i++)
                {
                    try
                    {
                        objConsultarRCE_BE.NroTicket = DtPendientes.Rows[i]["nroticket00"].ToString();
                        objConsultarRCE_BE.Contenedor = DtPendientes.Rows[i]["contenedor00"].ToString();
                        objConsultarRCE_BE.Tipo = DtPendientes.Rows[i]["Tipo00"].ToString();
                        objConsultarRCE_BE.Manifiesto = DtPendientes.Rows[i]["Manifiesto"].ToString();
                        objConsultarRCE_BE.Oficina = DtPendientes.Rows[i]["idOficina00"].ToString();

                        sTraza = viSucursal + "|" + objConsultarRCE_BE.NroTicket + "|" + objConsultarRCE_BE.Contenedor + "|" + objConsultarRCE_BE.Tipo + "|" + objConsultarRCE_BE.Manifiesto + "|" + objConsultarRCE_BE.Oficina;

                        otextwriter.WriteLine(DateTime.Now + "(" + viSucursal + "): Traza Enviada - " + sTraza);

                        //--Envio Informacion de salida a FAST
                        Response = ODSNeptFastNEP.ConsultarRCE(sUserFast, sPassFast, objConsultarRCE_BE.NroTicket, objConsultarRCE_BE.Oficina);

                        if (Response.Contains("descripcion"))
                        {
                            ResponseBE jsonObject = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseBE>(Response);

                            if (jsonObject.id.ToString() == "0")
                            {
                                objConsultarRCE_BE.Estado = jsonObject.descripcion.ToString();
                                objConsultarRCE_BL.UpdateEstadoTicketRCE(viSucursal, objConsultarRCE_BE.NroTicket, objConsultarRCE_BE.Estado);
                                otextwriter.WriteLine(DateTime.Now + "(" + viSucursal + "): ticket transmitido con exito, nro. ticket - " + objConsultarRCE_BE.NroTicket.ToString() + ", Estado - " + objConsultarRCE_BE.Estado.ToString());
                            }
                            else
                            {
                                otextwriter.WriteLine(DateTime.Now + "(" + viSucursal + "): Ticket - " + objConsultarRCE_BE.NroTicket.ToString() + " con error WS - " + jsonObject.descripcion.ToString().Trim());
                                sError = "(" + viSucursal + ")" + "Ticket - " + objConsultarRCE_BE.NroTicket.ToString() + "con error WS - " + jsonObject.descripcion.ToString().Trim();
                                objSendEmailBL.Send(sError, sTraza);
                            }
                        }
                        else
                        {
                            otextwriter.WriteLine(DateTime.Now + "(" + viSucursal + "): Ticket - " + objConsultarRCE_BE.NroTicket.ToString() + " con error WS - " + Response);
                            sError = "(" + viSucursal + ")" + "Ticket - " + objConsultarRCE_BE.NroTicket.ToString() + " con error WS - " + Response;
                            objSendEmailBL.Send(sError, sTraza);
                        }
                    }
                    catch (Exception ex)
                    {
                        sError = ex.Message.ToString().Trim();
                        otextwriter.WriteLine(DateTime.Now + "(" + viSucursal + "):Ticket (" + objConsultarRCE_BE.NroTicket.ToString().Trim() + " - " + ex.Message);
                        objSendEmailBL.Send(sError, sTraza);
                    }

                }
            }
        }

    }
}
